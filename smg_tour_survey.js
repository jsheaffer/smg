/**
 * SMG Survey - Tour
 * @author JS
 *
 * _@NApiVersion 2.x
 * _@NScriptType MapReduceScript
 */

define(['N/search', 'N/runtime', './smg_library', './smg_settings'], function( search, runtime, library, settings ) {

    // Functions
    function getInputData() {

        var script   = runtime.getCurrentScript();
        var list     = script.getParameter({ name: 'custscript_smg_tour_search' });

        log.audit({
            title: 'SMG Campaign',
            details: 'Campaign: tour, Search: ' + list
        });

        return search.load({
            id : list
        });
    }

    function reduce( context ) {

        var script   = runtime.getCurrentScript();

        log.debug({
            title: 'Reduce Function Usage - Start',
            details: script.getRemainingUsage()
        });

        log.debug({
            title: 'Reduce Context',
            details: context
        });

        var result = JSON.parse( context.values[0] );

        log.debug({
            title: 'Results',
            details: result
        });

        var parent = {
            type     : 'Parent',
            campaign : 'tour',
            id       : result.id,
            email    : result.values.email,
            school   : result.values.custentitymktloc.text,
            tourDate : library.getTourDate()
        };

        log.audit({
            title: 'SMG | Parent Record',
            details: parent
        });

        var success = settings.sendSurvey( parent );

        if ( success ) {

            library.createLog( parent );
        }

        log.debug({
            title: 'Reduce Function Usage - End',
            details: script.getRemainingUsage()
        });
    }


    function summarize( summary ) {

        log.audit({
			title: 'Summary',
			details: summary
		});
    }

    // Actions
	return {
		getInputData : getInputData,
        reduce       : reduce,
		summarize    : summarize
	};

});
