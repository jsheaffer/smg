SMG - Parent Surveys
===================================================


Purpose
---------------------------------------------------
Send data to SMG via the API to trigger a survey process controlled
by SMG. Parents will be surveyed on four different occasions: After they **tour** a school,
while they are **enrolled** in a school (twice a year), when they are **withdrawn** from a school,
and at the end of a **summer camp** program.  


Summary
---------------------------------------------------
Data for parents will be sent to SMG via their API. Scheduled Map/Reduce SuiteScripts
and Saved Searches will manage which parents are surveyed and when they are surveyed.


Contacts
---------------------------------------------------
* Justin Sheaffer - Spring IT - [justin.sheaffer@nlcinc.com](mailto:justin.sheaffer@nlcinc.com)
* Patty Laurento - NCLI Marketing - [patty.laurento@nlcinc.com](mailto:patty.laurento@nlcinc.com@nlcinc.com)
* Christine Kulp - NLCI Marketing - [Christine.Kulp@nlcinc.com](mailto:Christine.Kulp@nlcinc.com@nlcinc.com)
* Rachel Minter - SMG Account Manager


Components
---------------------------------------------------
### Records
* SMG Survey Schedule
* SMG Survey Log

### Searches
* SCRIPT USE | SMG | Camp List
* SCRIPT USE | SMG | Enrolled List
* SCRIPT USE | SMG | Withdrawn List
* SCRIPT USE | SMG | Tour List

### Scripts/Deployments
* SMG | Survey - Map/Reduce
    * SMG | Camp Contacts
    * SMG | Camp Parents
    * SMG | Enrolled Contacts
    * SMG | Enrolled Contacts - KC
    * SMG | Enrolled Parents
    * SMG | Enrolled Parents - KC
    * SMG | Withdrawn Contacts
    * SMG | Withdrawn Parents
* SMG | Tour Survey - Map/Reduce
    * SMG | Tour


Process
---------------------------------------------------
*The Enrolled, Camp and Withdrawn surveys search on the student, the map/reduce script then
maps all students to the parent or contact record before sending to SMG. This follows the
flow of the map/reduce script. The Tour survey searches on the parent as there is no student data involved.*

1. **Enrolled Survey** - The enrolled survey is sent twice a year during the active school year.
Parents and Contacts ( designated as Parent in the Relationship field ) are surveyed.  There are 4
separate deployments for this survey.  They are split out to Parents and Contacts and again for
Kids Campus (KC).  Kids Campus is done separately because they have different program requirements.
This survey is triggered by the SMG Survey Schedule record which is controlled by Spring Marketing.

2. **Camp Survey** - The camp survey is sent 4 days after a schools camp end date. This survey has two
deployments one each for Parents and Contacts.

3. **Withdrawn Survey** - The withdrawn survey runs daily and surveys parents whose status has switched
to withdrawn the previous day. This survey has two deployments one each for Parents and Contacts.

4. **Tour Survey** - The Tour survey runs daily and surveys parents who toured a school 7 days prior.
The search looks for parent status that switched from Inquiry to Tour 7 days ago.

The Map/Reduce script type starts with a search.  The process is then run for each search result.  If the
search is pulling students the students are grouped by their parent. The script builds out a JSON object (example below),
which is sent to SMG via an HTTPS POST request. If that request is successful a 'Survey Log' record is created.

*Enrolled, Camp, Withdrawn Data Sample*

    parent = {
        type         : 'Parent' or 'Contact',
        id           : NetSuite ID, internalid,
        email        : Parent Email, email,
        school       : School, # custentitymktloc,
        enrollDate   : Enrollment Date, custentity_orgenrolldt,
        students     : [
            {
                birthdate  : Student birthdate, custentity_birth_date,
                program    : Program, custrecorditemgroup.CUSTENTITY_PROGRAM,
                enrollDate : Enrollment Date, custentity_orgenrolldt
            }
        ],
        studentCount : Number of students in list
    }

*Tour-Data Sample*

    parent = {
        type         : 'Parent' or 'Contact',
        id           : NetSuite ID, internalid,
        email        : Parent Email, email,
        school       : School #, custentitymktloc,
        tourDate     : Tour Date, calculated as 7 days ago
    }

**Survey Log**

After a successful request is sent to SMG the process will create a 'Survey Log' record that
that records the internal id, survey type, and email that was used. This can be viewed on
the associated parent or contact record.
