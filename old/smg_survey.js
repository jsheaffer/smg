/**
 * Trigger AutoPay Script
 * @author JS
 *
 * _@NApiVersion 2.x
 * _@NScriptType MapReduceScript
 */

define(['N/search', 'N/runtime', './smg_library', './smg_settings'], function( search, runtime, library, settings ) {

    /**
    * Get parent records for SMG survey
    */
    function getInputData() {

        var script   = runtime.getCurrentScript();
        var list     = script.getParameter({ name: 'custscript_smg_search' });
        var campaign = script.getParameter({ name: 'custscript_smg_campaign' });

        log.audit({
            title: 'SMG Campaign',
            details: 'Campaign: ' + campaign + ', Search: ' + list
        });

        return search.load({
            id : list
        });
    }

    /**
    * Build SMG records for parents
    * @param {String} context  Value(parent) JSON string
    */
    function map( context ) {

        var script   = runtime.getCurrentScript();
        var campaign = script.getParameter({ name: 'custscript_smg_campaign' });

        log.debug({
            title: 'Map Function Usage - Start',
            details: script.getRemainingUsage()
        });

        if ( [ 'camp', 'parent' ].indexOf( campaign ) > -1 ) {

            var record = JSON.parse( context.value );

            //  Build Student
            student = {
                birthdate  : record.values.custentity_birth_date,
                program    : record.values["custrecorditemgroup.CUSTENTITY_PROGRAM"].text,
                enrollDate : record.values.custentity_orgenrolldt
            };

            //  Build Parent
            if ( ! contact ) {

                parent = library.determineContact( context.key, record.values["internalid.parentCustomer"].value, record.values["email.parentCustomer"] );
            }
            else {

                parent = record.values["internalid.parentCustomer"].value;
            }
        }



        log.debug({
            title: 'Map Function Usage - End',
            details: script.getRemainingUsage()
        });

        context.write( parent );
    }

    /**
    * Send Survey for each found parent
    * @param {String} context  Key(parent) Values(contacts) JSON string
    */
    function reduce( context ) {

        var script = runtime.getCurrentScript();
        var survey = script.getParameter({ name: 'custscript_smg_campaign' });

        log.debug({
            title: 'Reduce Function Usage - Start',
            details: script.getRemainingUsage()
        });

        var record = JSON.parse( context.value );
        var parent = library.getParent( record, survey );

        var contact_list = [];
        if ( survey !== 'tour' ) {

            contact_list = library.getContacts( parent );
        }

        var parent_list  = [];
        var parent       = JSON.parse( context.key );
        var contact_list = JSON.parse( context.values[0] );

        parent_list.push( parent );
        parent_list = parent_list.concat( contact_list );

        log.debug({
            title: 'Parent List',
            details: parent_list
        });

        for ( var i = 0; i < parent_list.length; i ++ ) {

            settings.sendSurvey( parent_list[ i ] );
        }

        log.debug({
            title: 'Reduce Function Usage - End',
            details: script.getRemainingUsage()
        });

        context.write( survey, parent );
    }

    function summarize( summary ) {

        log.audit({
			title: 'Summary',
			details: summary
		});
    }

    // Actions
	return {
		getInputData : getInputData,
        map          : map,
		reduce       : reduce,
		summarize    : summarize
	};

});
