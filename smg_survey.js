/**
 * SMG Surveys - Camp, Withdrawn, Enrolled Parent
 * @author JS
 *
 * _@NApiVersion 2.x
 * _@NScriptType MapReduceScript
 */

define(['N/search', 'N/runtime', './smg_library', './smg_settings'], function( search, runtime, library, settings ) {

    // Functions
    function getInputData() {

        var script    = runtime.getCurrentScript();
        var list      = script.getParameter({ name: 'custscript_smg_search' });
        var campaign  = script.getParameter({ name: 'custscript_smg_campaign' });
        var scheduled = true;

        if ( campaign === 'parent' ) {

            var result = search.load({
                id: 'customsearch_smg_survey_schedule'
            }).run().getRange({ start: 0, end: 1 });

            scheduled = ( result.length > 0 );
        }

        log.audit({
            title: 'SMG Campaign',
            details: 'Campaign: ' + campaign + ', Search: ' + list
        });

        if ( scheduled ) {

            return search.load({
                id : list
            });
        }
        else {

            log.audit({
                title: 'SMG | Surveys',
                details: 'No surveys scheduled'
            });
        }
    }

    function map( context ) {

        var script   = runtime.getCurrentScript();
        var contacts = script.getParameter({ name: 'custscript_smg_contacts' });
        var campaign = script.getParameter({ name: 'custscript_smg_campaign' });
        var kc       = script.getParameter({ name: 'custscript_smg_kids_campus' });

        log.debug({
            title: 'Map Function Usage - Start',
            details: script.getRemainingUsage()
        });

        log.debug({
            title: 'Context',
            details: context
        });

        //  Build student data
        var student = library.getStudent( context, campaign, kc );
        //  Determine Parent
        var parent  = library.determineParent( context, campaign, contacts );

        log.debug({
            title: 'Parent and student',
            details: 'Parent: ' + parent + ' - Student: ' + student
        });

        log.debug({
            title: 'Map Function Usage - End',
            details: script.getRemainingUsage()
        });

        if ( parent ) {

            context.write( parent, student );
        }
    }

    function reduce( context ) {

        var script   = runtime.getCurrentScript();
        var contacts = script.getParameter({ name: 'custscript_smg_contacts' });
        var campaign = script.getParameter({ name: 'custscript_smg_campaign' });

        log.debug({
            title: 'Reduce Function Usage - Start',
            details: script.getRemainingUsage()
        });

        log.debug({
            title: 'Reduce Context',
            details: context
        });

        //  Parse Student List
        var student_list = [];
        for ( var i = 0; i < context.values.length; i ++ ) {

            student_list.push( JSON.parse( context.values[ i ] ) );
        }

        var parent = library.getParent( context.key, student_list, campaign, contacts );

        log.audit({
            title: 'SMG | Parent Record',
            details: parent
        });

        var success = settings.sendSurvey( parent );

        if ( success ) {

            library.createLog( parent );
        }

        log.debug({
            title: 'Reduce Function Usage - End',
            details: script.getRemainingUsage()
        });
    }

    function summarize( summary ) {

        log.audit({
			title: 'Summary',
			details: summary
		});
    }

    // Actions
	return {
		getInputData : getInputData,
        map          : map,
		reduce       : reduce,
		summarize    : summarize
	};

});
