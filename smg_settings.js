/**
 * SMG Library functions
 * @author JS
 */

define(['N/encode', 'N/https', 'N/runtime'], function( encode, https, runtime ) {

//  Functions
    function getEnvironment() {

        switch ( runtime.envType ) {
            case 'PRODUCTION':
                return {
                    url     : "https://api.smg.com/transaction",
                    headers : {
                        "Consumer-Identifier" : "NobelTransactionAPI",
                        "Consumer-Secret-Key" : "zKqLM9d8WrKJG0LQwHNdaOn0yAxOE5qv7fVQhjZJi1I1Qjw0bI",
                        "Content-Type"        : "application/json"
                    }
                };
                break;

            case 'SANDBOX':
                return {
                    url     : "https://stage.api.smg.com/transaction",
                    headers : {
                        "Consumer-Identifier" : "NobelTestingAPI",
                        "Consumer-Secret-Key" : "TGbHeOeKS4X2W8yD5wvqPMuapwn1jNqt8RaOMLf8hN6EgDfTrQ",
                        "Content-Type"        : "application/json"
                    }
                };
                break;
        }
    }


    function sendSurvey( data ) {

        var smg = getEnvironment();

        try {

            var response = https.request({
                method  : "POST",
                url     : smg.url,
                body    : JSON.stringify( data ),
                headers : smg.headers,
            });

            var smg_response = JSON.parse( response.body );

            log.audit({
                title: 'SMG | Response',
                details: 'Transaction ID : ' + smg_response.Value + ' | ' + data.type + ' : ' + data.id + ' - ' + data.email
            });

            return true;
        }
        catch ( error ) {

            log.error({
                title: 'SMG Response',
                details: error
            });

            return false;
        }
    }

//	Actions
	return {
        sendSurvey : sendSurvey,
	};
});
