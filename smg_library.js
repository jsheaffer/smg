/**
 * SMG Library functions
 * @author JS
 */

define(['N/record', 'N/search', 'N/format'], function( record, search, format ) {

//  Functions

    function createLog( parent ) {

        var record_type = 'custrecord_survey_log_' + parent.type.toLowerCase();

        log.debug({
            title: 'Survey Log',
            details: parent
        });

        record.create({
            type : 'customrecord_survey_log'
        }).setValue({
            fieldId : record_type,
            value   : parent.id
        }).setValue({
            fieldId : 'custrecord_survey_log_email',
            value   : parent.email
        }).setValue({
            fieldId : 'custrecord_survey_log_type',
            value   : parent.campaign
        }).save();
    }


    function determineParent( context, campaign, contacts ) {

        var data;
        var result = JSON.parse( context.value );

        if ( campaign === 'withdrawn' ) {

            data = {
                parent_id    : context.key,
                parent_email : result.values.email,
                student_id   : result.values["internalid.subCustomer"].value
            };
        }
        else {

            data = {
                parent_id    : result.values["internalid.parentCustomer"].value,
                parent_email : result.values["email.parentCustomer"],
                student_id   : context.key
            };
        }

        if ( contacts ) {

            return getContact( data );
        }
        else {

            return data.parent_id;
        }

    }

    /**
    * Get SMG formatted Parent record
    * @param {String} parent_id
    * @param {Object} student_list - SMG formatted student list
    * @param {Boolean} contact - get contact info if true
    * @return {Object} SMG formatted data
    */
    function getParent( parent_id, student_list, campaign, contacts ) {

        if ( contacts ) {

            var parent_fields = search.lookupFields({
                type : search.Type.CONTACT,
                id   : parent_id,
                columns : [
                    'email',
                    'company.custentitymktloc'
                ]
            });

            var parent = {
                type         : 'Contact',
                id           : parent_id,
                email        : parent_fields.email,
                school       : adjustSchool( parent_fields["company.custentitymktloc"][0].text ),
                enrollDate   : getContactEnrollmentDate( student_list ),
            };
        }
        else {

            var parent_fields = search.lookupFields({
                type : search.Type.CUSTOMER,
                id   : parent_id,
                columns : [
                    'email',
                    'custentitymktloc',
                    'custentity_orgenrolldt'
                ]
            });

            var parent = {
                type         : 'Parent',
                id           : parent_id,
                email        : parent_fields.email,
                school       : parent_fields.custentitymktloc[0].text,
                enrollDate   : parent_fields.custentity_orgenrolldt,
            };
        }

        parent.campaign     = campaign;
        parent.students     = student_list;
        parent.studentCount = student_list.length;

        return parent;
    }

    /**
    * Get SMG formatted Student data
    * @param  {String} context  - json string from search result
    * @param  {String} campaing - smg campaign
    * @return {Object} SMG formatted data
    */
    function getStudent( context, campaign, kc ) {

        var result = JSON.parse( context.value );

        if ( campaign === 'withdrawn' ) {

            return {
                birthdate  : result.values["custentity_birth_date.subCustomer"],
                program    : '',
                enrollDate : result.values["custentity_orgenrolldt.subCustomer"]
            };
        }
        else {

            var program = ( kc ) ? '' : result.values["custrecorditemgroup.CUSTENTITY_PROGRAM"].text;

            //  Build Student
            return {
                birthdate  : result.values.custentity_birth_date,
                program    : program,
                enrollDate : result.values.custentity_orgenrolldt
            };
        }
    }


    function getContact( data ) {

        log.debug({
            title: 'Determine contact',
            details: 'Student ID: ' + data.student_id + ' - Primary Parent ID: ' + data.parent_id + ' - Primary Parent Email: ' + data.parent_email
        });

        var contact_search = search.load({
            id: 'customsearch_student_contacts'
        });
        contact_search.filters.push(
		    search.createFilter({ name: 'internalid', operator: search.Operator.IS, values: data.student_id }),
            search.createFilter({ name: 'company', operator: search.Operator.IS, values: data.parent_id, join: 'contact' }),
		    search.createFilter({ name: 'custentity_pickup_relationship', operator: search.Operator.IS, values: 4, join: 'contact' }),
            search.createFilter({ name: 'email', operator: search.Operator.ISNOTEMPTY, values: null, join: 'contact' }),
            search.createFilter({ name: 'email', operator: search.Operator.ISNOT, values: data.parent_email, join: 'contact' })
		);
        var results = contact_search.run().getRange({ start: 0, end: 1 });

        log.debug({
            title: 'search results',
            details: results
        });

        if ( results[0] ) {

            return results[0].getValue({ name: 'internalid', join: 'contact' });
        }
        else {

            return false;
        }
    }


    /**
    * Calculate Tour date as 7 days ago
    * @return {String} Formatted date string
    */
    function getTourDate() {

        var days = 7;
        var date = new Date();
        var last = new Date( date.getTime() - ( days * 24 * 60 * 60 * 1000 ) );

        var tour = format.format({
            value : last,
            type  : format.Type.DATE
        });

        return tour;
    }


    /**
    * Get original enrollment date for contact record
    * @param {Object} contact - SMG contact Record
    * @param {Object} parent  - SMG parent Record
    * @return {String} Formatted date string
    */
    function getContactEnrollmentDate( student_list ) {

        var dates = [];

        log.debug({
            title: 'Enrollment Dates',
            details: student_list
        });

        for ( var i = 0; i < student_list.length; i ++ ) {

            dates.push( student_list[ i ].enrollDate );
        }

        var date_order = function( date1, date2 ) {

            var a = new Date( date1 );
            var b = new Date( date2 );

            if (a > b) return 1;
            if (a < b) return -1;
            return 0;
        }

        dates.sort( date_order );

        return dates[ 0 ];
    }
    
    /**
	 * Parse School External Id from sales rep
	 * @param {String} text
	 * @return {String}
	 */
	function adjustSchool( school ) {

		var school_list = {
			'7001' : '7002',
			'1064' : '1061'
		};

		if ( school_list[ school ] ) {

			return school_list[ school ];
		}
		else {

			return school;
		}
	}


//	Actions
	return {
        createLog       : createLog,
        getParent       : getParent,
        getStudent      : getStudent,
        determineParent : determineParent,
        getTourDate     : getTourDate
	};
});
